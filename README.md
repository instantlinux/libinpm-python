# libinpm-python
With this library you can manage packages from inpm. Such as installing, uninstalling, updating, creating repository's or compiling packages.

## Dependencies
You need the following packages to use this library:
* termcolor (`sudo pip3 install termcolor`)
* brotlipy  (`sudo apt install brotli`)

## Example
Take a look at [iinstall.py](https://github.com/InstantCodee/libinpm-python/blob/master/iinstall.py). This is a simple script that access the inpm-python library and do the things overhead.
Use `python3.5 iinstall.py --help OR -h` to get help for the syntax.

## Install
Make sure that you have installed all dependencies!
1. `git clone https://github.com/InstantCodee/libinpm-python.git`
2. `cd libinpm-python`
3. `sudo pip3 install .`

## How to use this library
First, import the library:
```python
import libinpm
```

Now init the library:
```python
libinpm.init()
```

After this, open the database:
```python
db = libinpm.DatabaseManager()
db.openfile()
```
Now, you can start and using the library. But how? Read continue.

#### database.py
The [**database.py**](https://github.com/InstantCodee/libinpm-python/blob/master/database.py) is the head of the API.
The database can return all repository's, installed packages or all dependencies of a package.

`openfile()` is a void function.

`add(package)` is also a void function.

`remove(package)` is also a void function.

`getRepositories()` return's a string array of all repository's.

`getRepository(name)` return's a string array.

* 0 = URL to the md5 file.
* 1 = URL to the repository.

`isPkgInstalled()` return's **True** if it's installed and **False** if not.

`getAllDependencies(name)` this function return's a string array of package names.
This function is different as *repo.getDependencies(name)* because this function also return's the dependencies of the dependencies and so on.


#### repository.py
The [**repository.py**](https://github.com/InstantCodee/libinpm-python/blob/master/repository.py) is a single repository.
You can get a repository over the "database.py" with:
```python
db.getRepository(name) # Example: db.getRepository("base")
```
At the moment it's only a string. Let's convert it into a repository. Do this with:
```python
repo = libinpm.RepositoryManager(db.getRepository(repo)[1], repo)
```
Now, we have a object from type "repository".

We can combine this with a for-loop to get all repository's:
```python
for repo in db.getRepositories():
    repo = libinpm.RepositoryManager(db.getRepository(repo)[1], repo)
    ...
```

We can use now our repository to check if the repository is up to date, download it, get all packages in it, check if a package is in it, get dependencies of a package or get a package info.

We take a look at `packageInfo(name)`. This function return's a array of strings:
* 0 = repository
* 1 = name
* 2 = url
* 3 = version
* 4 = description
* 5 = image
* 6 = license
* 7 = md5
* 8 = dependencies

For example: `["base", "pkg1", "http://...inp", "1.0.0", "Example package", "http://...png", "GPL-3.0", "59965f5e1aafdb63698f8ae505daf864", "pkg1, pkg2"]`

`getPkg(name)` return's a object of type package (look below).

`getDependencies(name)` return's all dependencies for this package in form of a string array.

`hasPkg(name)` return's **True** if the package is in this repository and **False** if not.

`getAllPackages()` return's a string array of all packages inside the repository.

`downloadRepo()` return's **True** if the download was successfully and **False** when the repository is already up to date.

`isRepoUpToDate()` return's **True** if the repository is up to date and **False** when not.

#### package.py
The [**package.py**](https://github.com/InstantCodee/libinpm-python/blob/master/package.py) is a single package.

But how can I get a object as type package? It's simple:
```python
myPackage = repo.getPkg(pkgName)
```
To install a package, do:
```python
myPackage.download()    # This download the .inp file from the server.
myPackage.install()     # This will install the package.
db.add(myPackage)       # This will add the package to the db.sqlite file, so that no double installations happen.
```
To uninstall a package, write:
```python
myPackage.uninstall()
```
`download()` return's **True** if the download was successfully and **False** if not.

`install()` is a void function.

`uninstall()` is also a void function.

