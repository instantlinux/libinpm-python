import libinpm
import sys

from termcolor import colored

if sys.argv.__contains__("--help") or sys.argv.__contains__("-h"):
    print("Help to iinstall")
    print("--------------------------------")
    print("iinstall [Package/s] (-y)    -> Install one or more packages. (force yes)")
    print("iinstall -r [Package]        -> Removes one or more packages")
    print("iinstall -u                  -> Upgrade all packages.")
    print("iinstall -s                  -> Sync all repository's.")
    print("iinstall -c [Source]         -> Compile source.")
    print("iinstall -S [Package]        -> Search package in repository's.")
    print("iinstall -l                  -> List all packages.")
    print("iinstall -h                  -> Print's this message.")
    print("iinstall -C [Output] [Input] -> Creates a repository.")
    print("--------------------------------")
    sys.exit(0)

libinpm.init()  # This init the library and creates all folders if not exists.
db = libinpm.DatabaseManager()
db.openfile() # This opens the sqlite3 database.

# List all packages
if sys.argv[1] == "-l":
    for repo in db.getRepositories():
        repo = libinpm.RepositoryManager(db.getRepository(repo)[1], repo)
        for pkg in repo.getAllPackages():
            pkg = repo.getPkg(pkg)
            print(colored(pkg.getName(), 'green', attrs=["bold"]) + colored(" [" + pkg.getVersion() + "]", 'green'))
            print("    " + pkg.getDescription())

# Remove one or more packages.
if sys.argv[1] == "-r":
    toRemove = []
    possibly_not_needed = []
    for i in range(2, len(sys.argv)):
        toRemove.append(sys.argv[i])

    for pkg in toRemove:
        if not db.isPkgInstalled(pkg):
            print(colored("ERROR: This package is not installed!", 'red', attrs=["bold"]))
            sys.exit(0)
        rmpkg = libinpm.PackageManager(pkg, "", db.getInstalledVersion(pkg), "", "", "", "")
        print("")
        print(" --> " + colored(rmpkg.getName() + " [" + rmpkg.getVersion() + "]", 'red', attrs=['bold']))
        print("    - Uninstalling ...")
        rmpkg.uninstall()
        db.remove(rmpkg)
        print("")

# Installing one or more packages.
if not sys.argv.__contains__("-r") and not sys.argv.__contains__("-u") and not sys.argv.__contains__("-sync") and not sys.argv.__contains__("-c") and\
not sys.argv.__contains__("-s") and not sys.argv.__contains__("-l") and not sys.argv.__contains__("-h") and not sys.argv.__contains__("-C"):
    toInstall = []      # Contains all package names as string.
    packages = []       # Contains all packages as package type.
    dependencies = []   # Contains all dependencies for all packages.

    for s in range(1, len(sys.argv)):
        toInstall.append(sys.argv[s])

    # Search package/s in repository's.
    for pkg in toInstall:
        # Search for package
        for repo in db.getRepositories():
            repo = libinpm.RepositoryManager(db.getRepository(repo)[1], repo)
            if repo.hasPkg(pkg):
                if not db.isPkgInstalled(pkg):
                    # Convert str object to package object.
                    packages.append(repo.getPkg(pkg))

                # Check for dependencies
                for depend in db.getAllDependencies(pkg):
                    for repo in db.getRepositories():
                        repo = libinpm.RepositoryManager(db.getRepository(repo)[1], repo)
                        if repo.hasPkg(depend):
                            dependencies.append(repo.getPkg(depend))

    awnser = "y"
    if not sys.argv.__contains__("-y"):
        # Asking user to continue.
        if len(dependencies)+len(packages) == 0:
            print(colored("Everything is already installed! Nothing to do ...", 'green', attrs=["bold"]))
            sys.exit(0)

        print("The following packages will be installed(" + str(len(dependencies)+len(packages)) + "): ")

        print("  Packages(" + str(len(packages)) + "): ")
        sys.stdout.write("    ")
        for pkg in packages:
            sys.stdout.write(colored(pkg.getName() + " ", 'green'))

        print("\n  Dependencies(" + str(len(dependencies)) + "): ")
        sys.stdout.write("    ")
        for pkg in dependencies:
            sys.stdout.write(colored(pkg.getName() + " ", 'blue'))

        awnser = input("\n" + colored("Do you want to continue? [Y/n] : ", 'white', attrs=["bold"]))
    if awnser.lower() == "n":
        print("Aborting ...")
        sys.exit(0)
    elif awnser.lower() == "y" or awnser == "":
        # Installing all dependencies.
        for pkg in dependencies:
            if db.isPkgInstalled(pkg.getName()):
                print(colored(pkg.getName(), 'blue') + colored(": This package is already installed!", 'red'))
                break
            print(" --> " + colored(pkg.getName() + " [" + pkg.getVersion() + "]", 'blue', attrs=['bold']))
            print("    - Downloading ...")
            pkg.download()
            print("    - Installing ...")
            pkg.install()
            db.add(pkg)
            print("")

        # Installing all packages.
        for pkg in packages:
            if db.isPkgInstalled(pkg.getName()):
                print(colored(pkg.getName(), 'green') + colored(": This package is already installed!", 'red'))
                break
            print(" --> " + colored(pkg.getName() + " [" + pkg.getVersion() + "]", 'green', attrs=['bold']))
            print("    - Downloading ...")
            pkg.download()
            print("    - Installing ...")
            pkg.install()
            db.add(pkg)
            print("")
    else:
        print("Unknown awnser, aborting ...")
        sys.exit(0)

# Update all packages.
if sys.argv.__contains__("-u"):
    installed = db.getInstalledPackages()
    toInstall = []
    for repos in db.getRepositories():
        repo = libinpm.RepositoryManager(db.getRepository(repos)[1], repos)
        if not repo.downloadRepo():
            print(colored(repos + " is already up to date", 'green'))
        else:
            print(colored(repos + " is now synchronized.", 'green', attrs=['bold']))

        for pkg in installed:
            if repo.hasPkg(pkg):
                if repo.getPkg(pkg).getVersion() != db.getInstalledVersion(pkg):
                    toInstall.append(repo.getPkg(pkg))

    if len(toInstall) == 0:
        print(colored("Everything is up to date. Nothing to do...", 'green', attrs=['bold']))
        sys.exit(0)

    print("To update(" + str(len(toInstall)) + "):")
    for pkg in toInstall:
        sys.stdout.write(pkg.getName() + " ")

    awnser = input("\n" + colored("Do you want to continue? [Y/n] : ", 'white', attrs=["bold"]))

    if awnser.lower() == "n":
        print("Aborting.")
        sys.exit(0)
    if awnser.lower() == "y" or awnser == "":
        for pkg in toInstall:
            print(" --> " + colored(pkg.getName() + " [" + pkg.getVersion() + "]", 'yellow', attrs=['bold']))
            print("    - Downloading ...")
            pkg.download()
            print("    - Installing ...")
            pkg.install()
            db.add(pkg)