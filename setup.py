from setuptools import setup

setup(name='libinpm',
      version='0.0.1',
      description='With this library you can manage packages from inpm.',
      long_description='With this library you can manage packages from inpm. Such as installing, uninstalling, updating, creating repository\'s or compiling packages.',
      url='https://github.com/InstantCodee/libinpm-python',
      author='Mondei1',
      author_email='emailchecker000@gmail.com',
      license='GPL-3.0',
      requires=['termcolor'],
      packages=['libinpm'],
      zip_safe=False)