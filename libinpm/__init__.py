"""
This file is only checking all needed files or directory's.
If a file/directory not available, it's get created!
"""
import os, sys, shutil
from termcolor import colored

from libinpm.package import PackageManager
from libinpm.repository import RepositoryManager
from libinpm.database import DatabaseManager

__db = ""

def __isRoot():
    try:
        if os.path.isdir("/usr/share/inpm"):
            os.mkdir("/usr/share/inpm/testDir")
            os.rmdir("/usr/share/inpm/testDir")
            return True
        else:
            os.mkdir("/usr/share/inpm")
            return True
    except PermissionError:
        return False


def __createDirs():
    if not os.path.exists("/usr/share/inpm"): os.mkdir("/usr/share/inpm")
    if not os.path.exists("/usr/share/inpm/cache"): os.mkdir("/usr/share/inpm/cache")
    if not os.path.exists("/usr/share/inpm/repos"): os.mkdir("/usr/share/inpm/repos")

def __createFiles():
    if not os.path.exists("/usr/share/inpm/config.xml"):
        content = "<?xml version=\"1.0\" ?>\n\
<base>\n\
    <md5>https://www.guidedlinux.org/repo.md5</md5>\n\
    <url>https://www.guidedlinux.org/repo.inrepo</url>\n\
</base>"
        with open("/usr/share/inpm/config.xml", 'w+') as f:
            f.write(content)
            f.close()

def init():
    if not __isRoot():
        print(colored("ERROR: Please start this program with root!", 'red', attrs=['bold']))
        sys.exit(0)
    __createDirs()
    __createFiles()