import re
import sqlite3
import xml.etree.ElementTree as ET
import libinpm

class DatabaseManager:
    __sqliteFile = "/usr/share/inpm/db.sqlite"
    __con = ""
    __c = ""

    def openfile(self):
        self.__con = sqlite3.connect(self.__sqliteFile)
        self.__c = self.__con.cursor()
        self.__c.execute("CREATE TABLE IF NOT EXISTS pkgs(name shorttext, version shorttext);")

    def add(self, package):
        self.__c.execute("SELECT name FROM pkgs WHERE name='" + package.getName() + "'")
        if self.__c.fetchone() != None:
            self.__c.execute("UPDATE pkgs set version='" + package.getVersion() + "' WHERE name='" + package.getName() + "';")
            self.__con.commit()
            return

        self.__c.execute("INSERT INTO pkgs (name, version) VALUES ('" + package.getName() + "', '" + package.getVersion() + "');")
        self.__con.commit()
    def remove(self, package):
        self.__c.execute("DELETE FROM pkgs WHERE name='" + package.getName() + "';")
        self.__con.commit()
    def getRepositories(self):
        packageInfo = []
        with open("/usr/share/inpm/config.xml") as f:
            xml = f.read()

        tree = ET.fromstring(re.sub(r"(<\?xml[^>]+\?>)", r"\1<root>", xml) + "</root>")
        for table in tree.getiterator():
            for child in table:
                packageInfo.append(child.tag)
            break
        return packageInfo

    def getRepository(self, name):
        """
        0 = URL to MD5sum
        1 = URL to repository.
        """
        packageInfo = []
        with open("/usr/share/inpm/config.xml") as f:
            xml = f.read()

        tree = ET.fromstring(re.sub(r"(<\?xml[^>]+\?>)", r"\1<root>", xml) + "</root>")
        for table in tree.getiterator(name):
            for child in table:
                packageInfo.append(child.text)
            break

        return packageInfo

    def isPkgInstalled(self, name):
        self.__c.execute("SELECT name FROM pkgs WHERE name='" + name + "'")
        if self.__c.fetchone() != None:
            return True
        else:
            return False

    # This function returns the dependencies of the dependencies and so on.
    # getDependencies() returns only the dependencies for only one package.
    def getAllDependencies(self, name):
        dependencies = []
        try:
            extra_dependencies = []
            forward = True

            # Get all dependencies.
            for repo in self.getRepositories():
                repo = libinpm.RepositoryManager(self.getRepository(repo)[1], repo)
                if repo.hasPkg(name):
                    for depend in repo.getDependencies(name):
                        if self.isPkgInstalled(depend):
                            pass
                        else:
                            dependencies.append(depend)

            while forward:
                # Get extra dependencies.
                for pkg in dependencies:
                    for repo in self.getRepositories():
                        repo = libinpm.RepositoryManager(self.getRepository(repo)[1], repo)
                        if repo.hasPkg(pkg):
                            for depend in repo.getDependencies(pkg):
                                if self.isPkgInstalled(depend) or dependencies.__contains__(depend):
                                    pass
                                else:
                                    extra_dependencies.append(depend)

                # Check if they are extra dependencies.
                if len(extra_dependencies) > 0:
                    for exdepend in extra_dependencies:
                        if exdepend != None: dependencies.append(exdepend)
                    extra_dependencies.clear()
                else:
                    forward = False
        except IndexError:
            pass

        return dependencies

    def getInstalledVersion(self, name):
        self.__c.execute("SELECT version FROM pkgs WHERE name='" + name + "'")
        return str(self.__c.fetchone()).replace("(", "").replace(")", "").replace(",", "").replace("'", "")

    def getInstalledPackages(self):
        installed = self.__c.execute("SELECT name FROM pkgs").fetchall()
        final_installed = []

        for pkg in installed:
            pkg = str(pkg).replace("(", "").replace(")", "").replace(",", "").replace("'", "")
            final_installed.append(pkg)

        return final_installed