import xml.etree.ElementTree as ET

import hashlib
import os
import re
import shutil
import urllib3
import libinpm

class RepositoryManager:
    __url = ""
    __name = ""
    __md5sumFile = ""
    __repoPath = ""
    __find = '''.//*[@id='%ID%']'''
    __db = ""

    def __init__(self, url, name):
        self.__url = url
        self.__name = name
        self.__repoPath = "/usr/share/inpm/repo" + name + ".inrepo"

        self.__db = libinpm.DatabaseManager().openfile()

    def isRepoUpToDate(self):
        onlineMD5 = ""
        onlineMD5_url = ""

        # Download md5 file
        with open("/usr/share/inpm/config.xml") as f:
            xml = f.read()
        tree = ET.fromstring(re.sub(r"(<\?xml[^>]+\?>)", r"\1<root>", xml) + "</root>")

        for table in tree.getiterator(self.getName()):
            for child in table:
                if child.tag == "md5":
                    onlineMD5_url = child.text
                break

        http = urllib3.PoolManager()
        r = http.request('GET', onlineMD5_url, preload_content=False)
        with open("/tmp/" + self.getName() + ".md5", 'wb') as f:
            while True:
                data = r.read(1024)
                if not data:
                    break
                f.write(data)
        with open("/tmp/" + self.getName() + ".md5", "r") as f:
            for line in f.readlines():
                if not line == "": onlineMD5 = str(line).replace("\n", "")
            os.remove("/tmp/" + self.getName() + ".md5")

        # Calculating local md5sum
        md5 = hashlib.md5()
        with open("/usr/share/inpm/repos/" + self.getName() + ".inrepo", 'rb') as f:
            while True:
                data = f.read(1024)
                if not data:
                    break
                md5.update(data)
            if md5.hexdigest() == onlineMD5:
                return True
            else:
                return False

    def getName(self):
        return self.__name

    def getURL(self):
        return self.__url

    def downloadRepo(self):
        if os.path.isfile("/usr/share/inpm/repos/" + self.getName() + ".inrepo"):
            if self.isRepoUpToDate():
                return False
        http = urllib3.PoolManager()
        r = http.request('GET', self.__url, preload_content=False)

        with open("/tmp/" + self.getName() + ".inrepo", 'wb') as out:
            while True:
                data = r.read(1024)
                if not data:
                    break
                out.write(data)
        shutil.move("/tmp/" + self.getName() + ".inrepo", "/usr/share/inpm/repos/" + self.getName() + ".inrepo")
        return True

    def getAllPackages(self):
        packages = []
        with open("/usr/share/inpm/repos/" + self.getName() + ".inrepo") as f:
            xml = f.read()

        tree = ET.fromstring(re.sub(r"(<\?xml[^>]+\?>)", r"\1<root>", xml) + "</root>")

        for table in tree.getiterator():
            for child in table:
                if not child.tag == "root" or not child.tag =="name" or not child.tag == "url" or not child.tag == "version" or not child.tag == "description" or not child.tag == "image" or not child.tag == "license" or not child.tag == "":
                    packages.append(str(child.tag).replace("\n", ""))
            break
        # This return something like [pkg1, pkg2, pkg3, ...]
        return packages

    def hasPkg(self, name):
        packageInfo = self.getPackageInfo(name)
        try:
            if packageInfo[1] == "":
                return False
            else:
                return True
        except IndexError:
            return False

    def getDependencies(self, name):
        # This return something like [pkg1, pkg2, pkg3, ...]
        return str(self.getPackageInfo(name)[8]).split(",")

    def getPackageInfo(self, name):
        """
        0 = repository
        1 = name
        2 = url
        3 = version
        4 = description
        5 = image
        6 = licence
        7 = md5
        8 = dependencies
        """
        packageInfo = []
        packageInfo.append(self.getName())
        with open("/usr/share/inpm/repos/" + self.getName() + ".inrepo") as f:
            xml = f.read()
        tree = ET.fromstring(re.sub(r"(<\?xml[^>]+\?>)", r"\1<root>", xml) + "</root>")
        for table in tree.iter(name):
            for child in table:
                packageInfo.append(child.text)
            break
        if packageInfo == "":
            pass

        return packageInfo

    def getPkg(self, name):
        packageInfo = self.getPackageInfo(name)

        # This return's a object of type package.
        return libinpm.PackageManager(packageInfo[1], packageInfo[2], packageInfo[3], packageInfo[4], packageInfo[5], packageInfo[6], packageInfo[7])