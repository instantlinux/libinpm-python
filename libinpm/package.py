import hashlib
import os
import shutil
import subprocess
import tarfile
import libinpm
import urllib3
import brotli
import codecs
import time

class PackageManager:
    __url = ""
    __name = ""
    __version = ""
    __description = ""
    __image = ""
    __license = ""
    __md5 = ""
    __temp_Archive = ""
    __path_To_Temp_Folder = ""
    __path_To_Archive = ""

    __db = ""
    __skipped = False
    __downloadTry = 0

    def __init__(self, name, url, version, description, image, license, md5):
        self.__url = url
        self.__name = name
        self.__version = version
        self.__description = description
        self.__image = image
        self.__license = license
        self.__md5 = md5

        self.__temp_Archive = "/tmp/" + name + ".inp"
        self.__path_To_Temp_Folder = "/tmp/" + name
        self.__path_To_Archive = "/usr/share/inpm/cache/" + name + ".inp"
        self.__db = libinpm.DatabaseManager().openfile()

    # GET's ############
    def getURL(self):
        return  self.__url

    def getName(self):
        return self.__name

    def getVersion(self):
        return self.__version

    def getDescription(self):
        return self.__description

    def getImage(self):
        return self.__image

    def getLicense(self):
        return self.__license

    def getMD5(self):
        return self.__md5
    ####################

    # PRIVATE FUNCTIONS ####
    """
    How this works:
        1. Copy everything that is in the .inp archive into his right place.
    """
    def __copyRecursively(self):
        for root, dirs, files in os.walk("/tmp/" + self.getName() + "/"):
            for file in files:
                newPath = str.replace(root, "/tmp/" + self.getName() + "/data", "")
                try:
                    os.makedirs(newPath + "/")
                except Exception: pass
                shutil.copy(root + "/" + file, newPath + "/" + file) # Example: /tmp/pkg1/data/usr/share/example -> /usr/share/example
    ########################
    """
    How this works:
        1. Connect to the server (download url saved in __url)
        2. Download file into the /tmp/ folder.
        3. Check both md5sum's.
    """
    def download(self):
        onlineMD5 = ""
        http = urllib3.PoolManager()
        r = http.request('GET', self.__url, preload_content=False)

        # Download file from Server
        with open(self.__temp_Archive, 'wb') as out:
            while True:
                data = r.read(1024)
                if not data:
                    break
                out.write(data)
        # Release connection
        r.release_conn()

        # Check local md5-hash
        md5 = hashlib.md5()
        with open(self.__temp_Archive, 'rb') as f:
            while True:
                data = f.read(1024)
                if not data:
                    break
                md5.update(data)
            onlineMD5 = md5.hexdigest()

        # Check both md5sum's
        if not onlineMD5 == self.__md5:
            if self.__downloadTry == 3:
                print("Download is corrupt, again. Skip this package ...")
                self.__skipped = True
                return False
            print("Download is corrupt! Try again ...")
            self.__downloadTry += 1
            self.download()
        else:
            return True

    """
    How this works:
        1. Move downloaded package into /usr/share/inpm/cache/%name%.inp.
        2. Decompress brotli compressed file -> Output is a .tar file.
        3. Unpack .tar file into /tmp/
        4. Move everything into his right place.

    By unpacking a brotli archive and unpack a non-compressed .tar archive it's ~20% more efficient
    as by unpacking a .tar.gz.
    """
    def install(self):
        if self.__skipped:
            return
        shutil.move(self.__temp_Archive, self.__path_To_Archive)

        # Decompress brotli archive.
        subprocess.run(['brotli', '--input', self.__path_To_Archive, '--output', str(self.__path_To_Archive).replace('.inp', '.tar'), '--decompress'])

        tar = tarfile.open(str(self.__path_To_Archive).replace('.inp', '.tar'), "r:")
        tar.extractall("/tmp/")
        tar.close()

        self.__copyRecursively()
        self.clean()

    """
    How this works:
        1. Decompress brotli package into /usr/share/inpm/cache/%name%.tar
        2. Extract all files into /tmp/
        3. Replace /tmp/%name%/data with an empty string.
        4. Follow the path and delete the files there.
    """
    def uninstall(self):
        # Decompress brotli archive.
        subprocess.run(['brotli', '--input', self.__path_To_Archive, '--output',
                        str(self.__path_To_Archive).replace('.inp', '.tar'), '--decompress'])

        tar = ""
        try:
            tar = tarfile.open(str(self.__path_To_Archive).replace('.inp', '.tar'), "r:")
        except FileNotFoundError:
            print(".inp file not found. But I need it to uninstall it. Download it again ...")
            self.download()
            self.uninstall()

        tar.extractall("/tmp/")
        tar.close()

        for root, dirs, files in os.walk("/tmp/" + self.getName() + "/"):
            for file in files:
                newPath = str.replace(root, "/tmp/" + self.getName() + "/data", "") + "/" + file
                os.remove(newPath)
        os.remove(self.__path_To_Archive)
        self.clean()

    """
    How this works:
        1. Remove the temp archive after installation.
        2. Remove extracted .tar file.
    """
    def clean(self):
        shutil.rmtree("/tmp/" + self.getName())
        os.remove(str(self.__path_To_Archive).replace('.inp', '.tar'))

    """
    COMING SOON
    """
    def compile(self):
        print("I do nothing yet.")