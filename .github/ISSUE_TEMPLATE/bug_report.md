---
name: Bug report
about: Report a bug to help us.

---

**Describe the bug**
A clear and concise description of what the bug is.

**How to reproduce**
Steps to reproduce the behavior:
1. Go to '...'
2. Click on '....'
3. Scroll down to '....'
4. See error

**Expected behavior**
A clear and concise description of what you expected to happen.

**Attachments**
If applicable, add screenshots, videos or gif's to help explain your problem.

**Desktop:**
 - Linux version: [e.g. 4.16.13-74]
 - Architecture: [e.g. x86_64] 
 - Distribution: [e.g. InstantLinux, Ubuntu]

**Additional context**
Add any other context about the problem here.
